# Leica Camera 5.0 for Mi 9T/Redmi K20 (davinci) AOSP 13

### Cloning :
- Clone this repo in vendor/xiaomi/miuicamera in your working directory by :
```
git clone https://gitlab.com/helliscloser/vendor_xiaomi_miuicamera.git -b leica-5.0 vendor/xiaomi/miuicamera
```

Make these changes in **sm6150-common**

**sm6150.mk**
```
# MiuiCamera
$(call inherit-product-if-exists, vendor/xiaomi/miuicamera/config.mk)
```
## Credits

### Original mod - https://github.com/a406010503/Miui_Camera

## Support

### https://t.me/itzdfplayer_stash <br>

